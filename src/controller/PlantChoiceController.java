package controller;

import java.awt.Dimension;
import java.util.HashSet;

import javax.swing.ImageIcon;

import esseri.Pianta;
import file_manager.Giocatore;
import gui.PlantButton;
import gui.PlantChoicePanel;

/**
 * 
 * 
 * Controller del pannello della scelta delle piante da utilizzare.
 * 
 * @author Martino De Simoni
 * 
 */
/*
 * La classe implementa il pattern mvc e il controller.
 *
 * La classe � difficilmente riutilizzabile. Sarebbe pi� semplice ricominciare da capo e prendere a modello gli altri controller.
 *
 */
//BUGGY riga 49 circa: bottoni inizializzati per debug
public class PlantChoiceController extends PanelController<PlantChoicePanel>{

	final private Giocatore g;
	final private String gameMessage; 
	final private int maxPlant=3; //TODO a dir la verit� dovrebbe andare nei dati del giocatore. 
	
	/**
	 * 
	 * @param _terminationString Stringa da mettere come argomento di notifyMaster per notificargli una terminazione.
	 * @param _gameMessage       Stringa che il pannello associato ritorner� a notifyController(). Il controller notificher� al master.
	 * @param _g				 Il giocatore che sceglie le proprie piante.
	 * @param _master			 Il master di questo controller.
	 * @param maxSize			 Dimensione massima del frame.
	 */
	
	public PlantChoiceController( final String _terminationString, final String _gameMessage, final Giocatore _g,
			final AbstractMasterPanelController _master, final Dimension maxSize){
	
		g = _g;
		gameMessage = _gameMessage;
		panelID = _terminationString;
		master = _master;
		
		
		//Inizializzare i plantbuttons e il panel
		HashSet<PlantButton> bottoni = new HashSet<>();
	
		for (String p :  g.pianteSbloccate) { //TODO Dovrebbe essere una pianta, ma le piante non sono ancora arrivate al porto di bitbucket
			try {

				@SuppressWarnings("unchecked")
				final Class<? extends Pianta> nuovaPianta = (Class<? extends Pianta>) Class
						.forName("esseri." + p); 
				final Pianta pianta = (Pianta) nuovaPianta.newInstance();
				bottoni.add( new PlantButton( new ImageIcon ( pianta.getImg()),pianta.getSoliRichiesti(),p) );


			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		controlledPanel = new PlantChoicePanel(gameMessage, bottoni, gui.Utility.SFONDO, this, maxPlant, maxSize );
		
	}

	@Override
	public void slaveHasTerminated() {
		
		this.controlledPanel.setVisible(false);
		
	}

	@Override
	public void notifyController(String msg) {

		if(msg == gameMessage){
			if(controlledPanel.getChosenPlantSize()==this.maxPlant){
				
				this.master.notifyMaster(panelID, controlledPanel.getChosenPlants());
			}
		}
	}

	@Override
	public void run() {

		this.controlledPanel.setVisible(true);
	
	}
	
}
