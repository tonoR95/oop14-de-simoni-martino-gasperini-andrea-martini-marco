package controller;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.Timer;

import esseri.Attacco;
import esseri.Azione;
import esseri.Consumabili;
import esseri.Creazione;
import esseri.Essere;
import esseri.IEssere;
import esseri.Movimento;
import esseri.NessunAzione;
import esseri.NessunEssere;
import esseri.Pallino;
import esseri.Pianta;
import esseri.Posizione2D;
import esseri.Quadratino;
import esseri.TipoEssere;
import esseri.TipoTerreno;
import esseri.Zombi;
import file_manager.Giocatore;
import file_manager.LevelReaderImpl;
import file_manager.Livello;
import gui.GamePanel;
import gui.PlantButton;

/**
 * 
 * Controller del pannello di gioco. Le piante sono gi� state selezionate.
 * 
 * @author Martino De Simoni
 *
 */

@SuppressWarnings("unused")
public class GameController extends
		AbstractInsertionPanelController<Azione, GamePanel> implements
		ActionListener {

	/*
	 * Per adesso cols e rows sono impostati a valori di default. In una
	 * versione pi� sofisticata del gioco, il valore � specificato nel livello,
	 * e non � final.
	 */
	private final Integer COLS = 9;
	private final Integer ROWS = 5;

	private final String shovel = "shovelMessage";
	private final String creation = "creationMessage";
	private final Giocatore g;
	private final Quadratino[][] zolle = new Quadratino[ROWS][COLS];
	private final Pallino[][] pallini = new Pallino[ROWS][COLS];
	private final Livello livello;
	private Timer timer;

	private String actionType;
	private String pianta;

	private boolean[] tosaerbaMangiato = new boolean[ROWS];
	private boolean perso = false;
	private boolean vinto = false;

	private final List<PlantButton> plantButtons;
	private int soli = Utility.SOLI_A_INIZIO_LIVELLO;

	private int zombieRimasti;
	private int timeSinceStart = 0;

	public GameController(final String filePathDatiLivelli,
			final List<PlantButton> _plantButtons,
			final AbstractMasterPanelController _master, final Giocatore _g,
			final String _panelID) {

		livello = new LevelReaderImpl(filePathDatiLivelli).leggiDatiLivelli()
				.get(_g.livello);

		plantButtons = _plantButtons;

		master = _master;

		g = _g;

		this.panelID = _panelID;
		final AbstractInsertionPanelController<Azione, ? extends GamePanel> c = this;

		final List<BufferedImage> immaginiTerreno = new ArrayList<>();
		for(int i=0;i<1;i++){
			for(int j=0;j<COLS;j++){
				zolle[i][j] = new Quadratino(TipoTerreno.ACQUA,
						gui.Utility.ACQUA); // Solo il livello del cortile per
											// adesso
				zolle[i][j].setBeing(NessunEssere.getInstance());
				immaginiTerreno.add(zolle[i][j].getImg());
			}
		}
				
				
		for (int i = 1; i < ROWS; i++) {
			for (int j = 0; j < COLS; j++) {
				zolle[i][j] = new Quadratino(TipoTerreno.CORTILE,
						gui.Utility.ERBA); // Solo il livello del cortile per
											// adesso
				zolle[i][j].setBeing(NessunEssere.getInstance());
				immaginiTerreno.add(zolle[i][j].getImg());
			}
		}

		controlledPanel = new GamePanel(plantButtons, COLS, ROWS, c, shovel,
				immaginiTerreno);

	}

	@Override
	public void slaveHasTerminated() {

		this.controlledPanel.setVisible(false); // questo pannello non resta in
												// memoria

	}

	public void insert(Azione inserendum) {

		super.insert(inserendum);

	}

	// TODO devo ricollegare questo metodo a quello sotto
	public void notifyController(final String msg) {

		if (msg == shovel) { // Click sulla pala
			actionType = shovel;

		}

		boolean msgIsAPlantName = false;

		for (final PlantButton p : this.plantButtons) {
			if (p.getID().compareTo((String) msg) == 0) {
				msgIsAPlantName = true;

			}

		}

		if (msgIsAPlantName) { // Click sul bottone
			// � il nome di una pianta

			actionType = creation;
			for (final PlantButton p : this.plantButtons) {

				if (p.getID().compareTo((String) msg) == 0) {
					pianta = (String) msg;
				}

			}
		} else { // Click sulla griglia
					// la stringa � una posizione. Mi aspetto le coordinate
					// nella forma x + spazio + y
			final String blank = " ";
			final String[] coordinate = msg.split(blank);
			final Posizione2D pos = new Posizione2D(
					Integer.parseInt(coordinate[0]),
					Integer.parseInt(coordinate[1]));

			final int x = (int) pos.getX();
			final int y = (int) pos.getY();
			if (actionType == creation) {

				try {
					@SuppressWarnings("unchecked")
					final Class<? extends Pianta> nuovaPianta = (Class<? extends Pianta>) Class
							.forName("esseri." + pianta); // Mette la pianta di
															// cui ha preso
															// prima il nome
															// cliccando il
															// bottone (
															// richiamando
															// notifyController("Sparapallino"),
															// tipo)
					final Pianta pianta = (Pianta) nuovaPianta.newInstance();
					if (pianta.getSoliRichiesti() <= this.soli) {
						zolle[y][x]
								.setBeing((Pianta) nuovaPianta.newInstance());
						soli -= pianta.getSoliRichiesti();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			} else if (actionType == shovel) {

				// TODO togli una pianta, debug
				if (zolle[y][x].getContainedBeing().getTipoEssere() == TipoEssere.PIANTA) {
					eraseSquare(zolle[y][x]);
				}

			}
		}
	}

	protected void esaurisciAttacco(Attacco attacco) {

		final int x = (int) attacco.doveAttaccare.getX();
		final int y = (int) attacco.doveAttaccare.getY();

		if (areLegalCoordinates(x, y)
				&& zolle[y][x].getContainedBeing().getTipoEssere() == attacco.chiAttaccare) {

			zolle[y][x].getContainedBeing().prendiDanno(
					attacco.danno.doubleValue());

			if (zolle[y][x].getContainedBeing().isDead()) {

				zolle[y][x].getContainedBeing().eseguireAllaMorte();
				eraseSquare(zolle[y][x]);

			}

		} else
			esaudisciRichiesta(attacco.inCasoDiFallimento);

	}

	/**
	 * Esaurimento di una richiesa di movimento in orizzontale o verticale
	 */
	protected void esaurisciMovimento(final Movimento movimento) {

		final int x = (int) movimento.doveSpostarsi.getX();
		final int y = (int) movimento.doveSpostarsi.getY();
		final IEssere chiSiMuove = movimento.chiSiMuove;

		if (!(chiSiMuove instanceof Pallino)) {

			if (areLegalCoordinates(x, y) && zolle[y][x].canContain(chiSiMuove)) {

				zolle[y][x]
						.setBeing(zolle[(int) movimento.posizioneDiChiSiMuove
								.getY()][(int) movimento.posizioneDiChiSiMuove
								.getX()].getContainedBeing());
				eraseSquare(zolle[(int) movimento.posizioneDiChiSiMuove.getY()][(int) movimento.posizioneDiChiSiMuove
						.getX()]);

			}
			// TODO debug
			else if (movimento.chiSiMuove instanceof Zombi && x < 0) { // uno
																		// zombie
																		// arriva
																		// in
																		// fondo
																		// e si
																		// mangia
																		// un
																		// tosaerba

				if (!tosaerbaMangiato[(int) movimento.doveSpostarsi.getY()]) {
					tosaerbaMangiato[(int) movimento.doveSpostarsi.getY()] = true; // Gnam
				} else {
					perso = true; // Tosaerba gi� mangiato
				}
				eraseSquare(zolle[(int) movimento.posizioneDiChiSiMuove.getY()][(int) movimento.posizioneDiChiSiMuove
						.getX()]); // Lo zombie esce di scena

			} else
				esaudisciRichiesta(movimento.inCasoDiFallimento);

		}

		else if (chiSiMuove instanceof Pallino) { // Un pallino si sta muovendo

			if (!(zolle[(int) movimento.posizioneDiChiSiMuove.getY()][(int) movimento.posizioneDiChiSiMuove
					.getX()].getContainedBeing() instanceof Zombi)) {// Se non
																		// c'�
																		// uno
																		// zombie
																		// il
																		// pallino
																		// va
																		// avanti

				pallini[y][x] = pallini[(int) movimento.posizioneDiChiSiMuove
						.getY()][(int) movimento.posizioneDiChiSiMuove.getX()];
				pallini[(int) movimento.posizioneDiChiSiMuove.getY()][(int) movimento.posizioneDiChiSiMuove
						.getX()] = null; // TODO mettere
											// NessunEssere.getInstance() quando
											// la classe pallino sar� a posto

			} else {
				esaudisciRichiesta(movimento.inCasoDiFallimento); // Il pallino
																	// attacca
																	// lo zombie
																	// (?)
			}
		}

	}

	protected void esaurisciCreazione(final Creazione creazione) {

		final int x = (int) creazione.doveCreare.getX();
		final int y = (int) creazione.doveCreare.getY();
		if (!(creazione.cosaCreare instanceof Pallino)) {
			if (areLegalCoordinates(x, y) && zolle[y][x].isEmpty()) {

				boolean creato = zolle[y][x]
						.setBeing((IEssere) creazione.cosaCreare);
				if (creato && creazione.cosaCreare instanceof Zombi) {
					this.zombieRimasti++;
				}
			}
			// ma se � un pallino o un consumabile attacca comunque
			else if (areLegalCoordinates(x, y) && !zolle[y][x].isEmpty()
					&& creazione.cosaCreare instanceof Consumabili) {

				// Se non c'� spazio per il consumabile, questi comunque
				// attacca, ma senza aspettare. Sarebbe tutto pi� logico con le
				// animazioni
				creazione.cosaCreare.inviaAzione(
						creazione.cosaCreare.getTempoRichiesto(), this,
						new Posizione2D(x, y));

			}
		} else if (creazione.cosaCreare instanceof Pallino) {

			if (pallini[y][x] == null) { // Quando la classe Pallino sar� a
											// posto,
											// pallini[y][x]==NessunEssere.getInstance()
				pallini[y][x] = (Pallino) creazione.cosaCreare;
			}
		}

		else
			esaudisciRichiesta(creazione.inCasoDiFallimento);

	}

	/**
	 * 
	 * Esaudisce la richiesta espressa tramite l'azione inserita come parametro.
	 * esseriSenzienti e Pallini devono avere taglia uguale.
	 * 
	 * @param azioneGenerica
	 * @param zolle
	 * @param pallini
	 */
	/*
	 * Per implementare i minigiochi, � pi� facile sovrascrivere questa funzione
	 * che le subordinate rispettive
	 */
	protected void esaudisciRichiesta(final Azione azioneGenerica) {

		if (azioneGenerica instanceof NessunAzione) {
			// non fare nulla
		}

		else if (azioneGenerica instanceof Attacco) {
			esaurisciAttacco((Attacco) azioneGenerica);
		}

		else if (azioneGenerica instanceof Movimento) {
			esaurisciMovimento((Movimento) azioneGenerica);
		}

		else if (azioneGenerica instanceof Creazione) {
			esaurisciCreazione((Creazione) azioneGenerica);
		}

	}

	private void esaudisciRichieste() {
		// TODO le richieste vanno ordinate (col comparator) e esaudite in
		// ordine per evitare bug
		for (final Azione azioneGenerica : this.set) {

			esaudisciRichiesta(azioneGenerica);

		}

		set.clear(); // Tutte le azioni sono state eseguite
	}

	@Override
	public void run() {

		this.controlledPanel.setVisible(true);

		zombieRimasti = livello.numeroZombie;

		timer = new Timer(Utility.TICK, this);
		timer.setRepeats(true);
		timer.setInitialDelay(0);
		timer.start();
	}

	protected void eraseSquare(Quadratino q) {

		if (q.getContainedBeing().isDead()
				&& q.getContainedBeing() instanceof Zombi)
			zombieRimasti--;
		if (zombieRimasti <= 0 && !perso) {
			vinto = true;
		}
		q.makeEmpty();

	}

	protected boolean areLegalCoordinates(final int x, final int y) {

		return y < ROWS && y >= 0 && x < COLS && x >= 0;
	}

	private void updatePlayerData(final Giocatore g) {

		// TODO cambiare il livello, mettere piante disponibili in pi� se
		// bisogna..
		// Metodo vuoto per questa implementazione con un solo livello.

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		List<BufferedImage> immagini = new ArrayList<>(COLS * ROWS);

		/*
		 * Per ogni partita: All'inizio: Metto gli zombi all'inizio se devo
		 * Aspetto un tick Per ogni turno successivo: Metto gli zombi se ce ne
		 * sono Accolgo le richieste degli esseri se ce ne sono Aggiorno la GUI
		 * Aspetto un tick
		 * 
		 * Intanto: accolgo le richieste della GUI
		 */

		if (!vinto && !perso) {

			final int startTurnTime = (int) System.currentTimeMillis();
			boolean zombieNonAggiunto;
			immagini = new ArrayList<>();
			for (int y = 0; y < ROWS; y++) {
				for (int x = 0; x < COLS; x++) {
					zolle[y][x].getContainedBeing().inviaAzione(Utility.TICK, this,
							new Posizione2D(x, y));
				}
			}

			do { // metti altri zombie se � il momento

				if (!livello.zombie.isEmpty()
						&& livello.zombie.get(0).tempoInMs <= timeSinceStart) {

					try {
						@SuppressWarnings("unchecked")
						final Class<? extends Zombi> nuovoZombie = (Class<? extends Zombi>) Class
								.forName("esseri."
										+ livello.zombie.get(0).nomeZombie);
						zolle[livello.zombie.get(0).laneId - 1][COLS - 1]
								.setBeing((Zombi) nuovoZombie.newInstance());

					} catch (Exception e) {
						e.printStackTrace();
					}
					livello.zombie.remove(0);
					zombieNonAggiunto = false;
				}

				else
					zombieNonAggiunto = true;

			} while (!zombieNonAggiunto);

			esaudisciRichieste();
			// Aggiorno la GUI
			int i = 0;
			for (int y = 0; y < ROWS; y++)
				for (int x = 0; x < COLS; x++) {
					immagini.add(i, zolle[y][x].getContainedBeing().getImg());
					i++;
				}
			this.controlledPanel.updatePiante(immagini, this.soli);

			final int endTurnTime = (int) System.currentTimeMillis();
			final int turnTime = endTurnTime - startTurnTime;

			timeSinceStart += Utility.TICK;// C'abbiamo messo un tick,
											// idealmente. Facciamo finta di
											// niente nel remoto caso
											// contrario mi sembra il
											// miglior modo di garantire il
											// buon gameplay
		} else {
			timer.setRepeats(false);
			timer.stop();
			updatePlayerData(g);
			vinto = !perso; // Questo protegge il codice nel caso particolare in
							// cui L'ULTIMO ZOMBIE arriva in fondo a tosaerba
							// gi� mangiato, quindi il programma segna sia che
							// si � vinto che si � perso. Il codice � protetto
							// anche in altri modi.
			this.master.notifyMaster(this.panelID, vinto);

		}
	}

}
