package controller;

import file_manager.Giocatore;
import file_manager.NessunGiocatore;
import file_manager.UserDataManager;
import file_manager.UserDataManagerImpl;
import gui.MainFrame;
import gui.PlantButton;
import gui.Utility;

import java.awt.EventQueue;
import java.util.List;

import javax.swing.JPanel;


/**
 * 
 * 
 * Controller del frame principale (e unico in questo progetto), e master dei vari panelController.
 * 
 * @author Martino De Simoni
 */  
/*
 * Classe disegnata ad hoc e difficilmente riutilizzabile.
 * 
 */



public class FrameController extends AbstractMasterPanelController implements Runnable {

	//Stringhe-ID che il frameController passa ai PanelController per riconoscerli. Non devono essere n� visualizzate n� tradotte. A dir la verit� conta solo l'indirizzo, non il valore. Scelta a mio giudizio migliore delle enum.
	
	private final String menuMessage = "men�";
	private final String userChoiceMessage = "userChoice";
	private final String plantChoiceMessage = "scelta";
	private final String trueGameMessage = "game"; //Messaggio per il controller del gioco vero e proprio, senza la scelta delle piante n� niente
	private final String optionsMessage = "options";
	private final String exitMessage = "exit";
	
	//Oggetti per operare su file
	
	private Giocatore g = NessunGiocatore.NESSUN_GIOCATORE; //pu� cambiare a seconda della scelta utente. Importante non mettere questo campo a final
	private final UserDataManager dataManager;
	
	/**
	 * Inizializza il frame, il dataManager e i livelli.
	 */
	
	public FrameController(){
		
		frame = new MainFrame(Utility.TITLE,Utility.LOGO );
		dataManager = new UserDataManagerImpl( controller.Utility.FILE_DATI_UTENTE ); //deve davvero stare qui?

	}
	/**
	 * Alias di PlantsVsZombies
	 */
	
	@Override
	public void run() {
			plantsVsZombies();	
	}
	
	/**
	 * Inizio del gioco Piante contro Zombie.
	 * Ha il ruolo del metodo run nelle altre classi: raccoglie le istruzioni da eseguire all'avvio del controller. 
	 * Questo metodo � anche un alias di run.
	 */
	
	public void plantsVsZombies() {
		
		final PanelController<? extends JPanel> userChoice = new UserChoiceController( dataManager , frame, userChoiceMessage,this);
		slaves.put(userChoiceMessage,userChoice);
		
		frame.setMainPanel(slaves.get(userChoiceMessage).getControlledPanel(), true);
		frame.setVisible(true);
		slaves.get(userChoiceMessage).run();
		
	}	

	/*
	 * Ogni blocco che comincia con un if descrive la logica relativa al controller definito nella clausola.
	 * Per esempio, nel blocco if (msg==MenuMessage){} si definisce il comportamento del Pannello del men�.
	 * 
	 * Lascio notare che le stringhe non devono solo fare match, ma devono proprio essere la stessa.
	 */
	/**
	 * Gli slave notificano al master la loro terminazione, tramite l'ID che il master consegna loro, dai costruttori.
	 * 
	 *  @param msg 
	 *  			ID, passato dal master, del PanelController-slave che richiama il metodo.
	 *  @param args
	 *  		 	argomenti utili all'esecuzione.
	 */
	@Override
	public void notifyMaster(final String msg, final Object args) {
		
		if (msg==menuMessage){ //Siamo nel men�
			
			//Il secondo argomento � l'id del PanelController a cui si vuole passare
			if( (String) args == userChoiceMessage) {
				
				//cambio di utente: togli tutto
				slaves.clear(); 
				frame.removeAll();
				this.frame.setVisible(false);
				frame.dispose();
				//ricomincia da capo - easy but expensive way -
				new FrameController().plantsVsZombies(); 
				
			}
				
			else if ( (String) args == optionsMessage) {} //TODO Opzioni non implementate
			
			else if ( (String) args == plantChoiceMessage) {
				
				slaves.get(msg).slaveHasTerminated();
				
				final PlantChoiceController p = new PlantChoiceController(plantChoiceMessage, trueGameMessage, g, this, frame.getSize());
				frame.setMainPanel(p.controlledPanel, true);
				
				slaves.put((String) args, p);
				slaves.get((String)args).run();
				
				p.run(); 
				
				
			}
		
			else if ( (String) args == exitMessage) { 
				
				dataManager.aggiornaDatiGiocatore(g); //Dovrebbe davvero stare qui? Il dataManager deve stare nella classe di chi assegna il frame? O i controller possono inizializzare una parte del loro stato?
				System.exit(0);
				
			}
			
		} else if(msg==userChoiceMessage){  //Siamo al pannello di scelta utente
			//pu� andare solo al menu, il secondo argomento args � il giocatore selezionato

			slaves.get(userChoiceMessage).slaveHasTerminated();
			
			g = (Giocatore) args;
			
			
			final MenuController menu = new MenuController( g, menuMessage,plantChoiceMessage, optionsMessage,userChoiceMessage,exitMessage,this);
			slaves.put(menuMessage, menu);
			frame.setMainPanel( slaves.get(menuMessage).getControlledPanel(), true);
			slaves.get(menuMessage).run();
			
		} else if(msg==plantChoiceMessage){ //Siamo alla scelta delle piante, si pu� andare solo al gioco
			
			slaves.get(msg).slaveHasTerminated();
			slaves.remove(msg);
			
			@SuppressWarnings("unchecked")
			final GameController game = new GameController( controller.Utility.FILE_LIVELLI, (List<PlantButton>) args, this, g, trueGameMessage );
			slaves.put(trueGameMessage, game);
			frame.setMainPanel( slaves.get(trueGameMessage).getControlledPanel(), true);
			slaves.get(trueGameMessage).run();	
			
			
		} else if(msg==optionsMessage){
			//TODO Implementare le opzioni
			
		} else if(msg==plantChoiceMessage){
		
		// slaves
			
		} else if(msg==trueGameMessage){
			
			slaves.get(msg).slaveHasTerminated();
		//	slaves.remove(msg);
			//L'argomento � true solo se si ha vinto
			if((boolean) args) {
				System.out.println(Utility.VICTORY); //Messaggio di vittoria, versione povera. Andrebbe una form.
			}else{
				System.out.println(Utility.DEFEAT);
			}
			//Si passa al men�
			frame.setMainPanel( slaves.get(menuMessage).getControlledPanel(),true);
			slaves.get(menuMessage).run();
			
		}
	}

	
	//	 Per ora, il frame non ha bisogno di ricevere comunicazioni.

	@Override
	public void notifyController(final String msg) {
		
	}

	public static void main(final String args[]){
		
		 EventQueue.invokeLater(new Runnable(){
		     public void run(){
		         try{

		        	 new FrameController().plantsVsZombies();
		
		}catch(Exception e){
		             e.printStackTrace();
		         }

		     }});
		
	}
	
}
