/**
 * 
 */
package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/**
 * @author Andrea
 * 
 *         Peperone esplosivo colpisce gli zombi che si trovano sulla riga della
 *         matrice in cui questa pianta viene posizionata. Non colpisce tutta la
 *         riga , ma lo spazio indicato nel metodo faiRobe
 *
 */
public class PeperoneEsplosivo extends Consumabili {

	private static String img_filePath = "peperoneesplosivo.jpe";

	public PeperoneEsplosivo() {

		super(img_filePath, 0.1, Utility.PIANTA_DANNO_ALTO, 
				0, TipoTerreno.CORTILE);

	}

	public void inviaAzione(
			int tempo_trascorso,
			AbstractInsertionPanelController<Azione, ? extends JPanel> controller,
			Posizione2D posizione) {

		if (canAct(tempo_trascorso)) {
			if (!hoattaccato) {

				for (int i = 0; i <= 15; i++) {

					controller.insert(new Attacco(TipoEssere.ZOMBIE,
							new Posizione2D(-i, 0), danno, NessunAzione
									.getInstance(), posizione));
					controller.insert(new Attacco(TipoEssere.ZOMBIE,
							new Posizione2D(i, 0), danno, NessunAzione
									.getInstance(), posizione));

				}
				hoattaccato = true;

			} else
				controller.insert(new Attacco(TipoEssere.PIANTA,
						new Posizione2D(0, 0), this.life, NessunAzione
								.getInstance(), posizione));
		}

	}

	@Override
	public int getSoliRichiesti() {

		return 240;
	}

	@Override
	public TipoTerreno getTerreno() {

		return TipoTerreno.CORTILE;
	}

	@Override
	public void eseguireAllaMorte() {

	}

	@Override
	public boolean isDead() {

		return this.life <= 0;
	}

}
