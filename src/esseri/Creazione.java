package esseri;

/**
 * 
 * Comunica la creazione di un pallino, di un altro zombie..
 *
 * @author Martino De Simoni
 *
 */

/*
 * Non ci sono meccanismi di sicurezza per impedire al programmatore di far creare a una pianta uno zombie. 
 */

public class Creazione extends Azione {

	public final Essere cosaCreare;
	public final Posizione2D doveCreare;
    
    public Creazione(Essere essere,Azione incasodifallimento,Posizione2D doveCreare){
    	
    	super(incasodifallimento);
		this.doveCreare=doveCreare;
		this.cosaCreare = essere;
	}
    
}