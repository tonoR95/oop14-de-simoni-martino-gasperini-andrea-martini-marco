/**
 * 
 */
package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/**
 * @author Andrea Sparapallino estende l'astratta Offensivo , e nel metodo
 *         faiRobe , crea il pallino .
 *
 */
public class Sparapallino extends Offensivo {

	private static String img_filePath = "sparapallino.jpe";
	boolean attaccato = false;

	public Sparapallino() {

		super(img_filePath, Utility.PIANTA_VITA_BASSA, 0.0,
				Utility.PIANTA_TEMPO_BASSO, TipoTerreno.CORTILE);
	}

	public void inviaAzione(
			int tempo_trascorso,
			AbstractInsertionPanelController<Azione, ? extends JPanel> controller,
			Posizione2D posizione) {

		if (canAct(tempo_trascorso)) {

			controller.insert(new Creazione(new Pallino(), NessunAzione
					.getInstance(), new Posizione2D(1, 0)));

		}

	}

	@Override
	public int getSoliRichiesti() {

		return 100;
	}

	@Override
	public TipoTerreno getTerreno() {

		return TipoTerreno.CORTILE;
	}

	@Override
	public void eseguireAllaMorte() {

	}

	@Override
	public boolean isDead() {

		return this.life <= 0;
	}

}
