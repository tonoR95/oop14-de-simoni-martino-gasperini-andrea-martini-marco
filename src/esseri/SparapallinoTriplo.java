/**
 * 
 */
package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/**
 * @author Andrea Questa classe svolge gli stessi compiti di SparapallinoDoppio
 *         , ma ne crea un terzo
 */
public class SparapallinoTriplo extends Offensivo {

	private static String img_filePath = "sparapallinotriplo.jpe";

	public SparapallinoTriplo() {

		super(img_filePath, Utility.PIANTA_VITA_BASSA, 0.0,
				Utility.PIANTA_TEMPO_BASSO, TipoTerreno.CORTILE);
	}

	public void inviaAzione(
			int tempo_trascorso,
			AbstractInsertionPanelController<Azione, ? extends JPanel> controller,
			Posizione2D posizione) {

		if (canAct(Utility.PIANTA_TEMPO_BASSO)) {

			controller.insert(new Creazione(new Pallino(), NessunAzione
					.getInstance(), new Posizione2D(1, 0)));
			controller.insert(new Creazione(new Pallino(), NessunAzione
					.getInstance(), new Posizione2D(1, 0)));
			controller.insert(new Creazione(new Pallino(), NessunAzione
					.getInstance(), new Posizione2D(1, 0)));

		}
	}

	@Override
	public int getSoliRichiesti() {
		return 200;
	}

	@Override
	public TipoTerreno getTerreno() {
		return TipoTerreno.CORTILE;
	}

	@Override
	public void eseguireAllaMorte() {

	}

	@Override
	public boolean isDead() {

		return this.life <= 0;
	}

}
