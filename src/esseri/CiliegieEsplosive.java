/**
 * 
 */
package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/**
 * @author Andrea
 *
 *         CiliegieEsplosive ha un attacco elevato e colpisce gli zombie vicini
 *         , come mostra il metodo faiRobe
 *
 *
 */
public class CiliegieEsplosive extends Consumabili {

	private static String img_filePath = "ciliegieesplosive.jpe";

	public CiliegieEsplosive() {

		super(img_filePath,Utility.PIANTA_VITA_BASSA, Utility.PIANTA_DANNO_ALTO, 
				100, TipoTerreno.CORTILE);

	}

	public void inviaAzione(
			int tempo_trascorso,
			AbstractInsertionPanelController<Azione, ? extends JPanel> controller,
			Posizione2D posizione) {

		if (canAct(tempo_trascorso)) {

			controller.insert(new Attacco(TipoEssere.ZOMBIE, new Posizione2D(0,
					0), danno, NessunAzione.getInstance(), posizione));
			controller.insert(new Attacco(TipoEssere.ZOMBIE, new Posizione2D(0,
					1), danno, NessunAzione.getInstance(), posizione));
			controller.insert(new Attacco(TipoEssere.ZOMBIE, new Posizione2D(1,
					0), danno, NessunAzione.getInstance(), posizione));
			controller.insert(new Attacco(TipoEssere.ZOMBIE, new Posizione2D(
					-1, 1), danno, NessunAzione.getInstance(), posizione));
			controller.insert(new Attacco(TipoEssere.ZOMBIE, new Posizione2D(1,
					-1), danno, NessunAzione.getInstance(), posizione));
			controller.insert(new Attacco(TipoEssere.ZOMBIE, new Posizione2D(
					-1, 0), danno, NessunAzione.getInstance(), posizione));
			controller.insert(new Attacco(TipoEssere.ZOMBIE, new Posizione2D(0,
					-1), danno, NessunAzione.getInstance(), posizione));
			controller.insert(new Attacco(TipoEssere.ZOMBIE, new Posizione2D(
					-1, -1), danno, NessunAzione.getInstance(), posizione));
			controller.insert(new Attacco(TipoEssere.ZOMBIE, new Posizione2D(1,
					1), danno, NessunAzione.getInstance(), posizione));
			controller.insert(new Attacco(TipoEssere.PIANTA, new Posizione2D(0,
					0), this.life, NessunAzione.getInstance(), posizione));

		}

	}

	@Override
	public int getSoliRichiesti() {

		return 250;
	}

	@Override
	public TipoTerreno getTerreno() {

		return TipoTerreno.CORTILE;
	}

	@Override
	public void eseguireAllaMorte() {

	}

	@Override
	public boolean isDead() {

		return this.life <= 0;
	}

}
