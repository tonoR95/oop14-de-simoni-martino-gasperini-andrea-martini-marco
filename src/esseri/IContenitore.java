package esseri;

/**
 * Interfaccia per i contenitori: le zolle, i vasi, le ninfee..
 * @author Martino De Simoni
 *
 */
/*
 * Una soluzione per il metodo faiRobe delle PiantaContenitrice pu� essere: 
 * 	
 * 			faiRobe(args){ this.getContainedBeing().faiRobe(args);}
 * 
 * */
public interface IContenitore {

	public boolean canThisInParticularContain(IEssere e); 
	public boolean canContain(IEssere e);
	public boolean setBeing(IEssere e);   //restituisce lo stesso valore di canContain, ma ha ovviamente dei side effect
	public IEssere getContainedBeing();
}
