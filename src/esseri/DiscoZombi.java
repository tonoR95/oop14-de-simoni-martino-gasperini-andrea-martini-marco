
package esseri;

import javax.swing.JPanel;

import esseri.ZombiBallerino;
import controller.AbstractInsertionPanelController;

/*Lo zombi disco ha la peculiarita che quando viene evocato crea 4 zombiBallerini nel campo. 
 * 
 */

/*
 * @author Marco
 * 
 */

/**
 * 
 * @author Marco Martini
 *
 */


public class DiscoZombi extends Zombi {

private boolean inCampo = false;	
private static String img_filePath = "zombi_disco.jpeg";
	
	public DiscoZombi(){
		
		super(img_filePath,Utility.ZOMBIE_VITA_BASSA,Utility.ZOMBIE_DANNO_BASSO, Utility.ZOMBIE_TEMPO_BASSO, TipoTerreno.CORTILE);
		
	}


	public void inviaAzione(int tempo_trascorso,AbstractInsertionPanelController<Azione, ? extends JPanel > controller,Posizione2D posizione) {
		
		
		if(this.inCampo == false){
	
			controller.insert(new Creazione( new ZombiBallerino() ,NessunAzione.getInstance(),new Posizione2D(posizione.getX()-1,posizione.getY())));
			controller.insert(new Creazione( new ZombiBallerino() ,NessunAzione.getInstance(),new Posizione2D(posizione.getX(),posizione.getY()-1)));
			controller.insert(new Creazione( new ZombiBallerino() ,NessunAzione.getInstance(),new Posizione2D(posizione.getX(),posizione.getY()+1)));
			controller.insert(new Creazione( new ZombiBallerino() ,NessunAzione.getInstance(),new Posizione2D(posizione.getX()+1,posizione.getY())));
		
			this.inCampo = true;
			
		}	
			if(canAct(tempo_trascorso)){
				
				Attacco azioneDaImmettere = new Attacco( TipoEssere.PIANTA, new Posizione2D(-1,0),this.danno,new Movimento( new Posizione2D(-1,0), posizione,this, NessunAzione.getInstance() ),posizione);
				
			
				controller.insert( azioneDaImmettere );
			}
		
		
	}

	
	public boolean getInCampo(){
		
		return this.inCampo;
		
		
	}

	
	
}
