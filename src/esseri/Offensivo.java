package esseri;

/**
 * @author Andrea L'astratta Offensivo implementa i metodi utilizzati dalle
 *         pianti che la estendono
 *
 */
// classe astratta offensivo che implementa i metodi per tutte le piante
// offensive del programma
public abstract class Offensivo extends Pianta implements IOffensivo {

	public Offensivo(final String path, final Double _life, Double _danno,
			final Integer _tempo_richiesto, final TipoTerreno _terreno) {
		super(path, _life, _danno, _tempo_richiesto, _terreno);
	}

}