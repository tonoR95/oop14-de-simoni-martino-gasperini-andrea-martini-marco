package esseri;

import java.awt.image.BufferedImage;

/**
 *  Un quadratino della griglia di gioco di Piante contro Zombie.
 *  Quindi la zolla di terra, il quadrato di acqua della piscina, la porzione di tetto..
 * 
 *  @author Martino De Simoni
 * 
 */

public class Quadratino implements IContenitore {

	public final TipoTerreno terreno;
	protected IEssere essereContenuto = NessunEssere.getInstance();
	private final BufferedImage img;
	//Qualche filepath lo si pu� anche mettere qui, ma non � che una comodit�.
	//Altri quadratini da usare nella versione completa del gioco sono il cortile di notte, la piscina e il tetto.
	//Quadratini per versioni estese sono quelli dei minigiochi
	
	
	/**
	 * Il costruttore da usare per la versione completa del gioco.
	 * @param _terreno
	 * @param filePath
	 */
	public Quadratino(TipoTerreno _terreno, BufferedImage _img ){
		
		this.terreno = _terreno;
		img = _img;
	}
	
	public boolean canThisInParticularContain(IEssere e){
		
		if( this.isEmpty() && e.getTerreno()==this.terreno ) return true; 
		return false;
		
	}
	
	public void makeEmpty(){
		
		this.essereContenuto = NessunEssere.getInstance();
		
	}
	
	public boolean setBeing(IEssere e){
		
		if(canThisInParticularContain(e)) {
			
			this.essereContenuto = e;
			return true;
		
		}
		
		if (essereContenuto instanceof IContenitore) 
				return ( (IContenitore) this.getContainedBeing() ).setBeing(e);
		
		return false;
		
	}
	
	public boolean canContain(IEssere e){
		
		if( this.canThisInParticularContain(e) ) return true; //zolla libera
		
		if (essereContenuto instanceof IContenitore) 
			return ( (IContenitore) this.getContainedBeing() ).canThisInParticularContain(e); //zolla con un contenitore (un vaso, una ninfea..)
		return false; //zolla occupata
		
		
	}

	@Override
	public IEssere getContainedBeing() {
		return essereContenuto;
	}
	
	public boolean isEmpty(){
		
		return essereContenuto == NessunEssere.getInstance() || essereContenuto == null;
		
	}
	
	public boolean isOccupied(){
		
		return !isEmpty();
		
	}
	public BufferedImage getImg() {
		return img;
	}
	
}
