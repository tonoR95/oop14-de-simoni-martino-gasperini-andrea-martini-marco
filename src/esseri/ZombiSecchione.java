
package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/*
 * Lo zombiSecchione ha la peculiarità di essere piu forte di uno zombi normale e piu resistente
 * 
 */

/**
 * 
 * @author Marco Martini
 *
 */


public class ZombiSecchione extends Zombi implements IZombi {

	private static String img_filePath = "zombi_secchione.jpeg";
	
	public ZombiSecchione(){
		
		super(img_filePath,Utility.ZOMBIE_VITA_MEDIA,Utility.ZOMBIE_DANNO_MEDIO, Utility.ZOMBIE_TEMPO_MEDIO, TipoTerreno.CORTILE);
		
	}



	public void inviaAzione(int tempo_trascorso,AbstractInsertionPanelController<Azione, ? extends JPanel > controller, Posizione2D posizione) {
			
		if(canAct(tempo_trascorso)){
		
			Attacco azioneDaImmettere = new Attacco( TipoEssere.PIANTA, new Posizione2D(-1,0),this.danno,new Movimento( new Posizione2D(-1,0), posizione,this, NessunAzione.getInstance() ),posizione);
			
		
			controller.insert( azioneDaImmettere );
		}
	}





}
