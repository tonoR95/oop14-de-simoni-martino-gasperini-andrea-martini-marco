
package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/*
 * Lo zombi ballerino viene evocato in gruppi di 4 quando entra in gioco disco zombi
 * ed è solo un po piu veloce di uno zombi normale.
 * 
 */
/**
 * 
 * @author Marco Martini
 *
 */



public class ZombiBallerino extends Zombi {

	
	

private static String img_filePath = "zombi_ballerino.jpeg";
	
	public ZombiBallerino(){
		
		super(img_filePath,Utility.ZOMBIE_VITA_MEDIA,Utility.ZOMBIE_DANNO_BASSO, Utility.ZOMBIE_TEMPO_MEDIO, TipoTerreno.CORTILE);
		
	}



	public void inviaAzione(int tempo_trascorso,AbstractInsertionPanelController<Azione, ? extends JPanel > controller, Posizione2D posizione) {
			
		if(canAct(tempo_trascorso)){
		
			Attacco azioneDaImmettere = new Attacco( TipoEssere.PIANTA, new Posizione2D(-1,0),this.danno,new Movimento( new Posizione2D(-1,0), posizione,this, NessunAzione.getInstance() ),posizione);
			
		
			controller.insert( azioneDaImmettere );
		}
	}





}
