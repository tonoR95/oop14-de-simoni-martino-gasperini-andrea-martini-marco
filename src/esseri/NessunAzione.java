package esseri;

/**
 * 
 * @author Martino De Simoni
 *
 */

public class NessunAzione extends Azione {

	private static final NessunAzione nessunAzione = new NessunAzione(); // pattern
																			// Singleton

	public static NessunAzione getInstance() {

		return nessunAzione;

	}

	private NessunAzione() {
	}

}
