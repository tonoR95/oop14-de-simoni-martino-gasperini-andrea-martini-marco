package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/*
 * Lo zombi segnaletico ha un cono sopra la testa che lo rende due volte piu resistente dello zombi comune
 */

/**
 * 
 * @author Marco Martini
 *
 */


public class ZombiSegnaletico extends Zombi {

	
private static String img_filePath = "zombi_segnaletico.jpeg";
	
	public ZombiSegnaletico(){
		
		super(img_filePath,Utility.ZOMBIE_VITA_ALTA,Utility.ZOMBIE_DANNO_BASSO, Utility.ZOMBIE_TEMPO_BASSO, TipoTerreno.CORTILE);
		
	}



	public void inviaAzione(int tempo_trascorso,AbstractInsertionPanelController<Azione, ? extends JPanel > controller, Posizione2D posizione) {
			
		if(canAct(tempo_trascorso)){
		
			Attacco azioneDaImmettere = new Attacco( TipoEssere.PIANTA, new Posizione2D(-1,0),this.danno,new Movimento( new Posizione2D(-1,0), posizione,this, NessunAzione.getInstance() ),posizione);
			
		
			controller.insert( azioneDaImmettere );
		}
	}



}
