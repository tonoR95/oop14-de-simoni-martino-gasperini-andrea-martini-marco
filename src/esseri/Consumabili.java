package esseri;

/**
 * @author Andrea
 * 
 * 
 *         Consumabili � una classe astratta che contiene i metodi usabili nelle
 *         classi che la estendono (esempio CiliegieEsplosive)
 *
 */
// classe astratta che implementa i metodi delle piante consumabili del
// programma
public abstract class Consumabili extends Pianta implements IConsumabili {

	boolean hoattaccato = false;

	public Consumabili(final String path, final Double _life,
			final Double _danno, final Integer _tempo_richiesto,
			final TipoTerreno _terreno) {

		super(path, _life, _danno, _tempo_richiesto, _terreno);

	}
}