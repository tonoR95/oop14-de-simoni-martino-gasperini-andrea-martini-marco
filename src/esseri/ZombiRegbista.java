package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/*
 * Lo zombi regbista è lo zombi piu forte e resistente della nostra applicazione.
 * 
 */

/**
 * 
 * @author Marco Martini
 *
 */


public class ZombiRegbista extends Zombi {

	
private static String img_filePath = "zombi_regbista.jpeg";
	
	public ZombiRegbista(){
		
		super(img_filePath,Utility.ZOMBIE_VITA_ALTA,Utility.ZOMBIE_DANNO_ALTA, Utility.ZOMBIE_TEMPO_ALTO, TipoTerreno.CORTILE);
		
	}



	public void inviaAzione(int tempo_trascorso,AbstractInsertionPanelController<Azione, ? extends JPanel > controller, Posizione2D posizione) {
			
		if(canAct(tempo_trascorso)){
		
			Attacco azioneDaImmettere = new Attacco( TipoEssere.PIANTA, new Posizione2D(-1,0),this.danno,new Movimento( new Posizione2D(-1,0), posizione,this, NessunAzione.getInstance() ),posizione);
			
		
			controller.insert( azioneDaImmettere );
		}
	}




	
}
